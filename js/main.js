import { Board, Game } from './engine.js';
import { ButtonBar, BoardWidget } from './ui.js';

document.addEventListener('DOMContentLoaded', ()=>{
    let toolbar = new ButtonBar('tools'),
        keypad = new ButtonBar('keypad'),
        engine = new Game(),
        widget = new BoardWidget(engine);
    
    let pencil = false;
    for(let i=1; i<=9; i++){
        keypad.addTextButton(i, ()=>{
            if(widget.pos){engine[pencil ? 'toggleCandidate' : 'setValue'](widget.pos, i);
            }else{widget.markCells(i);}
        }, ()=>widget.marked==i, 'pencil');
    }
    keypad.addTextButton('×', ()=>{if(widget.pos) engine.setValue(widget.pos, 0)});
    toolbar.addImageButton({url: './res/btn_pencil.svg', text: 'Pencil', className: 'pencil'}, ()=>{pencil=!pencil}, ()=>pencil, 'pencil');
    toolbar.addImageButton({url: './res/btn_undo.svg', text: 'Undo'}, ()=>engine.undo(), ()=>engine.history.cur>=0);
    toolbar.addImageButton({url: './res/btn_redo.svg', text: 'Redo'}, ()=>engine.redo(), ()=>engine.history.cur<engine.history.edits.length-1);
    toolbar.addImageButton({url: './res/btn_check.svg', text: 'Check'}, ()=>{widget.showErrors=!widget.showErrors; widget.update();}, ()=>widget.showErrors, 'pencil');
    toolbar.addImageButton({url: './res/btn_snapshot.svg', text: 'Snap'}, ()=>{});
    
    document.body.appendChild(widget.container);
    document.body.appendChild(keypad.container);
    document.body.appendChild(toolbar.container);
    
    engine.registerCallback(()=>toolbar.update());
    engine.registerCallback(()=>widget.update());
    engine.generateBoard(42 /*Math.random()*30|0+1*/);
    
});
