export class ButtonBar{
    constructor(className){
        this.container = document.createElement('div');
        this.container.className = 'buttonbar';
        this.references = [];
        if(className) this.container.classList.add(className);
    }
    addRawButon(elem, callback, status, className){
        let tmp = document.createElement('span');
        if(status){
            this.references.push({elem: tmp, status: status});
            let m = callback;
            callback = ()=>{m(); this.update()};
        }
        tmp.appendChild(elem);
        tmp.addEventListener('mousedown', (e)=>{e.preventDefault()});
        tmp.addEventListener('click', callback);
        if(className) tmp.classList.add(className);
        this.container.appendChild(tmp);
    }
    addImageButton(data, callback, status, className){
        let tmp = document.createDocumentFragment(),
            img = document.createElement('img');
        img.src = data.url;
        tmp.appendChild(img);
        tmp.appendChild(document.createTextNode(data.text||''));
        if(data.alt) tmp.alt = data.alt;
        this.addRawButon(tmp, callback, status, className);
    }
    addTextButton(text, callback, status, className){
        let tmp = document.createTextNode(text)
        this.addRawButon(tmp, callback, status, className);
    }
    update(){
        for(let ref of this.references){
            ref.elem.classList.remove('active', 'inactive');
            ref.elem.classList.add(ref.status() ? 'active': 'inactive');
        }
    }
}

export class BoardWidget{
    constructor(engine){
        this.engine = engine;
        this.container = document.createElement('div');
        this.canvas = document.createElement('canvas');
        this.scale = Math.ceil(window.devicePixelRatio);
        this.pos = undefined;
        this.marked = undefined;
        this.showErrors = true;
        this.__curStyle = undefined;
        
        this.container.appendChild(this.canvas);
        this.canvas.addEventListener('click', (e)=>this.clickCallback(e));
        window.addEventListener('resize', ()=>this.update());
        
        this.style = {
            thickLine: 1, thinLine: 0.4 // percentage of container's max(width, height)
        };
    }
    get curStyle(){
        if(this.__curStyle) return this.__curStyle;
        let width = this.canvas.width|0,
            height = this.canvas.height|0,
            cnt = getComputedStyle(this.container),
            s = {}, dim = Math.min(width, height)|0;
        s.thickLine = dim*this.style.thickLine/100|0;
        s.thinLine = dim*this.style.thinLine/100|0;
        s.cellDim = (dim-4*s.thickLine-6*s.thinLine)/9|0;
        s.gridDim = s.cellDim*9+4*s.thickLine+6*s.thinLine;
        s.offsetX = (width-s.gridDim)/2|0;
        s.offsetY = (height-s.gridDim)/2|0;
        
        s.gridColor = cnt.getPropertyValue('--grid-color') || '#000000';
        s.textColor = cnt.getPropertyValue('--text-color') || '#000000';
        s.textAltColor = cnt.getPropertyValue('--text-alt-color') || '#FFFFFF';
        s.accentColor = cnt.getPropertyValue('--accent-color') || '#6200c8';
        s.font = cnt.fontFamily || 'sans-serif';
        
        return this.__curStyle = s;
    }
    getCellOffset(r, c){
        let s = this.curStyle,
            celldim = s.cellDim+s.thinLine;
        return {
            x: s.offsetX+c*celldim+(c/3|0)*(s.thickLine-s.thinLine)+s.thickLine,
            y: s.offsetY+r*celldim+(r/3|0)*(s.thickLine-s.thinLine)+s.thickLine
        };
    }
    clickCallback(e){
        let x = (e.clientX+document.documentElement.scrollLeft-e.target.offsetLeft)*this.scale,
            y = (e.clientY+document.documentElement.scrollTop-e.target.offsetTop)*this.scale,
            s = this.curStyle, squaredim = s.cellDim*3+s.thinLine*2+s.thickLine,
            nx = x-s.offsetX-s.thickLine, ny = y-s.offsetY-s.thickLine,
            squareh = nx/(squaredim)|0, squarev = ny/(squaredim)|0,
            r = 3*squarev+(ny-squarev*squaredim)/(s.cellDim+s.thinLine)|0,
            c = 3*squareh+(nx-squareh*squaredim)/(s.cellDim+s.thinLine)|0,
            cell = this.getCellOffset(r, c);
        
        let between = (v, max) => v[0] > 0 && v[0] < max && v[1] > 0 && v[1] < max,
            equal = (a, b) => a[0] == b[0] && a[1] == b[1];
            
        let outside = !between([nx, ny], s.gridDim),
            alreadyselected = equal([r, c], this.pos ? [this.pos.r, this.pos.c] : [-1, -1]),
            onaline = !between([x-cell.x, y-cell.y], s.cellDim),
            fixedcell = this.engine.board.isCellMasked(r, c);
        
        if(outside || alreadyselected || onaline || fixedcell){
            this.pos = undefined;
        }else{
            this.pos = {r: r, c: c};
        }
        this.marked = undefined;
        this.update();
    }
    paint(){
        let s = this.curStyle,
            ctx = this.canvas.getContext('2d');
        
        ctx.fillStyle = s.gridColor;
        for(let i=0; i<=9; i++){
            let cell = this.getCellOffset(i, i),
                thickness = i % 3 == 0 ? s.thickLine : s.thinLine;
            ctx.fillRect(s.offsetX, cell.y-thickness, s.gridDim, thickness);
            ctx.fillRect(cell.x-thickness, s.offsetY, thickness, s.gridDim);
        }
        for(let cell of this.engine.board.getMasked()){
            let off = this.getCellOffset(cell.r, cell.c);
            ctx.fillRect(off.x, off.y, s.cellDim, s.cellDim);
        }
        let marked = this.pos ? [this.pos] : this.engine.search(this.marked) || [],
            thick = (s.cellDim*0.3|0),
            tmp = s.cellDim+s.thinLine,
            corner = (x, y, a, b) => {
                ctx.fillRect(x, y, thick*a, s.thickLine*b);
                ctx.fillRect(x, y, s.thickLine*a, thick*b);
            };
        
        ctx.fillStyle = s.accentColor;
        for(let cur of marked){
            let off = this.getCellOffset(cur.r, cur.c);
            corner(off.x-s.thinLine, off.y-s.thinLine, 1, 1);
            corner(off.x+tmp, off.y-s.thinLine, -1, 1);
            corner(off.x+tmp, off.y+tmp, -1, -1);
            corner(off.x-s.thinLine, off.y+tmp, 1, -1);
        }
        
        ctx.font = s.cellDim+'px '+s.font;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'top';
        let candidates = [],
            wrong = this.showErrors ? this.engine.checkAll().map((x)=>x.r*9+x.c) : [];
        for(let r=0; r<9; r++){
            for(let c=0; c<9; c++){
                let off = this.getCellOffset(r, c),
                    cur = this.engine.board.data[r][c];
                if(Array.isArray(cur)){
                    candidates.push([off, cur]);
                }else if(cur > 0){
                    if(this.engine.board.isCellMasked(r, c)){
                        ctx.fillStyle =  s.textAltColor;
                    }else if(wrong.includes(r*9+c)){
                        ctx.fillStyle =  s.accentColor;
                    }else{
                        ctx.fillStyle = s.textColor;
                    }
                    ctx.fillText(cur, off.x+s.cellDim/2, off.y);
                }
            }
        }
        
        ctx.font = s.cellDim/3+'px '+s.font;
        ctx.fillStyle = s.textColor;
        for(let [off, cur] of candidates){
            for(let i of cur){
                let [r, c] = [(i-1)/3|0, (i-1)%3];
                ctx.fillText(i, off.x+c*s.cellDim/3+s.cellDim/6, off.y+r*s.cellDim/3);
            }
        }
    }
    update(){
        let real_width = parseFloat(getComputedStyle(this.container).width)|0;
        this.canvas.width = real_width*this.scale|0;
        this.canvas.height= real_width*this.scale|0;
        this.canvas.style = 'width: '+real_width+'px';
        
        this.__curStyle = undefined;
        this.paint();
    }
    markCells(value){
        this.marked = this.marked != value ? value : undefined;
        this.pos = undefined;
        this.update();
    }
}

