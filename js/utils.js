// simple timer using monotonic clock
export class Stopwatch{
    constructor(){
        this.offset = 0;         // time offset used to add up pauses
        this.begin = undefined;  // undefined whe not running or value of monotonic clock when started
    }
    
    start(){
        if(this.begin) return;
        this.begin = performance.now();
    }
    stop(){
        this.begin = undefined;
        this.offset = 0;
    }
    pause(){
        if(!this.begin) return;
        this.offset += performance.now() - this.begin;
        this.begin = undefined;
    }
    
    get value(){
        return !this.begin ? this.offset : performance.now()-this.begin+this.offset;
    }
    set value(offset){
        this.stop();
        this.offset = offset;
    }
}

export class RNG{
    constructor(value=1){
        this.state = value;
    }
    getBetween(a, b){
        return (this.value/RNG.MAX_VALUE*(b-a+1)|0)+a
    }
    get value(){
        let t = this.state;
        t ^= t << 13;
        t ^= t >> 17;
        t ^= t << 5;
        this.state = t;
        return t&RNG.MAX_VALUE;
    }
    
    static get MAX_VALUE(){
        return 0x7FFFFFFF;
    }
}
