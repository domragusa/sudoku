import { RNG, Stopwatch } from './utils.js';

export class Board{
    constructor(){
        this.data = undefined;
        this.mask = undefined;
        this.autoremovecandidates = false;
        
        this.init();
    }
    
    init(){
        this.data = [];
        for(let r=0; r<9; r++){
            this.data.push(Array(9).fill(0));
        }
        this.mask = [];
    }
    
    setValue(pos, value){
        if(this.isCellMasked(pos.r, pos.c)) return;
        if(value<0 || value>9) value = 0;
        
        this.setCell(pos.r, pos.c, value);
        if(this.autoremovecandidates)
            this.updateCandidates(pos.r, pos.c, value);
//         this.updateHistory();
    }

    updateCandidates(r, c, value){
        if(this.isCellMasked(r, c)) return;
        let upd = (x) => Array.isArray(x) ? x.filter((i)=> i!=value) : x;
        
        this.forEachNeighbor({r:r, c:c}, (i, j)=>{
            this.setCell(i, j, upd(this.getCell(i, j)));
        });
    }
    
    toggleCandidate(pos, value){
        let cur = this.getCell(pos.r, pos.c);
        if(!Array.isArray(cur)){cur = [];}
        cur = new Set(cur);
        
        if(cur.has(value)){
            cur.delete(value);
        }else{
            cur.add(value);
        }
        
//         this.updateHistory();
        this.setCell(pos.r, pos.c , [...cur]);
    }
    
    // helper function to loop through all cells
    // if fn returns:
    //  - 1 break from inner loop
    //  - 2 break from both loops (exit)
    forEachCell(fn){
        let ret=0;
        for(let r=0; r<9 && ret<2; r++){
            for(let c=0; c<9 && ret<1; c++){
                ret = fn(r, c, this.data[r][c]) | 0;
            }
        }
    }
    // helper function to loop through neighbor cells
    // if fn returns a negative value, the loop breaks
    forEachNeighbor(pos, fn){
        let cells = new Set(),
            sr = (pos.r/3|0)*3, sc = (pos.c/3|0)*3;
        for(let i=0; i<9; i++)
            cells.add(pos.r*9+i)
                 .add(i*9+pos.c)
                 .add((sr+i/3|0)*9+sc+i%3);
        cells.delete(pos.r*9+pos.c); // exclude current cell
        for(let x of cells)
            if(fn(x/9|0, x%9)|0<0) break;
    }
    
    setCell(r, c, value){
        this.data[r][c] = value;
    }
    getCell(r, c){
        return this.data[r][c];
    }
    getMasked(){
        return this.mask.map((x)=>({r: x/9|0, c: x%9}));
    }
    isCellMasked(r, c){
        return this.mask.includes(r*9+c);
    }
    setCellMasked(r, c){
        this.mask.push(r*9+c);
    }
}

export class Game{
    constructor(){
        this.time = new Stopwatch();
        this.board = new Board();
        this.solved = undefined;
        this.editMode = undefined;
        this.boardMode = undefined;
        this.history = undefined;
        this.callbacks = [];
        this.init();
    }
    init(){
        this.board.init();
        this.time.stop();
        this.solved = undefined;
        this.editMode = 1;
        this.boardMode = 1;
        this.history = {
            cur: -1,
            edits: [],
            last_saved: deepCopy(this.board.data)
        };
    }
    
    registerCallback(cb){
        this.callbacks.push(cb); 
    }
    notify(){
        for(let cb of this.callbacks){
            cb();
        }
    }
    
    start(){
        this.paused = false;
        this.time.start();
    }
    pause(){
        this.paused = true;
        this.time.pause();
    }
    toggleCandidate(pos, value){
        this.board.toggleCandidate(pos, value);
        this.updateHistory();
        this.notify();
    }
    setValue(pos, value){
        this.board.setValue(pos, value);
        this.updateHistory();
        this.notify();
    }
    // return all the detected collisions
    checkAll(){
        let res = [];
        this.board.forEachCell((r, c) => {
            if(this.checkOne(r, c)) res.push({r: r, c: c});
        });
        return res;
    }
    
    // check for collisions of the specified cell
    checkOne(r, c){
        let cur = this.board.getCell(r, c),
            sr = ((r/3)|0)*3, sc = ((c/3)|0)*3;
        
        // skip if empty or with candidates
        if(cur == 0 || Array.isArray(cur)) return false;
        
        let collides = false;
        this.board.forEachNeighbor({r:r, c:c}, (i, j)=>{
            if(this.board.getCell(i, j) == cur){
                collides = true;
                return -1;
            }
        });
        
        return collides;
    }
    
    search(value){
        let res = [];
        this.board.forEachCell((r, c, cur) => {
            if(!Array.isArray(cur)) cur = [cur];
            if(cur.includes(value)) res.push({r: r, c: c});
        });
        
        return res;
    }
    
    generateBoard(seed){
        console.log(seed);
        let rng = new RNG(seed);
        let perm = (n) => {
            let el = [...Array(n).keys()], res = [];
            for(let i=0; i<n; i++)
                res.push(...el.splice(rng.getBetween(0, el.length-1), 1));
            return res;
        },
        rotateRow = (row, pos) => [...row.slice(pos), ...row.slice(0, pos)],
        shuffleBands = (board) =>{
            let pmr = [3, 3, 3].map(perm);
            return board.map((_, i, rows) =>{
                let rr = i/3|0;
                return rows[pmr[rr][i%3]+rr*3];
            });
        },
        mirrorBoardD = (board) => board.map((x,r)=>x.map((_,c)=>board[c][r])),
        mirrorBoardV = (board) => board.map((_, r)=>board[8-r]),
        shiftBoard = (board, dir) => board.map((r)=>rotateRow(r, dir)),
        generateFromRow = (row) =>{
            let res = [];
            for(let i=0; i<9; i++){
                res.push(row);
                if(i%3 == 2)
                    row = row.map( (_,j) => row[j-j%3+(j+1)%3] );
                row = rotateRow(row, 3);
            }
            return res;
        }
        
        // wip
        let tmp = [1,2,3,4,5,6,7,8,9];
        let mask = perm(12).slice(-4).map((i)=>[(i/6|0), (i%6)]);
        mask = mask.concat(perm(6).slice(-3).map((i)=>[2, (i%6)]));
        mask = mask.concat(perm(6).slice(-2).map((i)=>[3, (i%6)]));
        mask = mask.concat(mask.map((x)=>[8-x[1], 8-x[0]]));
        mask = mask.concat(mask.map((x)=>[x[1], x[0]]));
        mask = mask.map( (x)=>({r:x[0], c:x[1]}) );
        
        this.init();
        this.loadBoard((generateFromRow(tmp)), mask);
    }
    
    undo(){
        if(this.paused) return;
        let h = this.history;
        if(h.cur < 0) return;
        
        for(let e of h.edits[h.cur]){
            this.board.data[e.r][e.c] = e.prev;
        }
        h.cur--;
        this.notify();
    }
    
    redo(){
        if(this.paused) return;
        let h = this.history;
        if(h.cur >= h.edits.length-1) return;
        
        h.cur++;
        for(let e of h.edits[h.cur]){
            this.board.data[e.r][e.c] = e.next;
        }
        this.notify();
    }
    
    updateHistory(){
        let h = this.history;
        if(h.cur < h.edits.length-1){
            h.edits = h.edits.slice(0, h.cur+1);
        }
        
        let tmp = [];
        this.board.forEachCell((r, c, cur) => {
            let last = h.last_saved[r][c];
            if(cur != last+''){
                tmp.push({r: r, c: c, prev: last, next: cur});
            }
        });
        if(!tmp.length) return;
        
        h.last_saved = deepCopy(this.board.data);
        h.edits.push(tmp);
        h.cur++;
    }
    loadBoard(board, mask){
        this.init();
        this.solved = board;
        mask.forEach((pos)=>this.board.setCellMasked(pos.r, pos.c));
        this.board.forEachCell((r, c)=>{
            if(this.board.isCellMasked(r, c))
                this.board.data[r][c] = board[r][c];
        });
        this.history.last_saved = deepCopy(this.board.data);
        this.notify();
    }
    load(data){
        let tmp = JSON.parse(data);
        this.time.value  = tmp.time;
        this.board.data  = tmp.board;
        this.board.mask  = tmp.mask;
        this.board.pos   = tmp.pos;
        this.solved      = tmp.solved;
        this.history     = tmp.history;
        this.notify();
    }
    store(){
        return JSON.stringify({
            time:    this.time.value,
            history: this.history,
            board:   this.board.data,
            mask:    this.board.mask,
            solved:  this.solved,
            pos:     this.board.pos
        });
    }
}

function deepCopy(x){
    return JSON.parse(
        JSON.stringify(x)
    );
}
