let cur_version = 'cache_v7';
 
let url_to_cache = [
    '/sudoku/',
    '/sudoku/index.html',
    '/sudoku/manifest.json',
    '/sudoku/style.css',
    '/sudoku/js/sudoku.js',
    '/sudoku/images/sym_timer.svg',
    '/sudoku/images/btn_erase.svg',
    '/sudoku/images/btn_redo.svg',
    '/sudoku/images/btn_undo.svg',
    '/sudoku/images/btn_pencil.svg',
    '/sudoku/images/btn_reload.svg',
    '/sudoku/images/icon-512.png',
    '/sudoku/images/icon-192.png',
    '/sudoku/images/icon.svg'
];


self.addEventListener('install', (e)=>{
    e.waitUntil(
        caches.open(cur_version).then((cache)=>cache.addAll(url_to_cache))
    );
});

self.addEventListener('fetch', (e)=>{
    e.respondWith(
        caches.match(e.request).then(
            (r)=> r || fetch(e.request)
        )
    );
});

self.addEventListener('activate', (e)=>{
    e.waitUntil(caches.keys().then(
        (names)=>Promise.all(
            names.map((cur)=>{
                if(cur != cur_version)
                    return caches.delete(cur);
            })
        )
    ));
});
